package Controller;

import Model.Polinome;
import View.InputPanel;
import View.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This Class is the main controller, which interracts with the Model and the GUI elements
 */
public class Controller {
    MainFrame mainFrame;
    InputPanel inputPanel;
    Polinome polinom1;
    Polinome polinom2;

    public Controller() {
        inputPanel = new InputPanel();
        ;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                mainFrame = new MainFrame("Calculator", inputPanel);
                mainFrame.setSize(500, 350);
                mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                mainFrame.setVisible(true);
                mainFrame.setResizable(false);
                mainFrame.setCalculateBtnListener(new CalculateBtnActionListener());
            }
        });

    }

    class CalculateBtnActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            get1stPolinom();
            get2ndPolinom();
            String operation = inputPanel.getoperationsSelectBtn();
            mainFrame.setTextarea("Operation Selected ~[ " + operation + " ]~\n" + "Result: \n");
            switch (operation) {
                case "Display":
                    mainFrame.appendToTextarea(polinom1 + "\n" + polinom2 + "\n");
                    break;
                case "Addition":
                    mainFrame.appendToTextarea(polinom1.add(polinom2) + "\n");
                    break;
                case "Substract":
                    mainFrame.appendToTextarea(polinom1.subtract(polinom2) + "\n");
                    break;
                case "Derivate":
                    mainFrame.appendToTextarea(polinom1.derivate() + "\n");
                    break;
                case "Multiply":
                    mainFrame.appendToTextarea(polinom1.multiply(polinom2) + "\n");
                    break;
                case "Integrate":
                    mainFrame.appendToTextarea(polinom1.integrate() + "\n");
                    break;
            }
        }
    }

    private void get2ndPolinom() {
        try {
            polinom2 = new Polinome(inputPanel.getPolValue2());
        } catch (NumberFormatException invalidFormat) {
            Component message = null;
            JOptionPane.showMessageDialog(message,
                    "Invalid Input for 2nd Polinom ~[ " + inputPanel.getPolValue1() + " ]~");
        } catch (ArrayIndexOutOfBoundsException a) {
            Component message = null;
            JOptionPane.showMessageDialog(message, "Error." +
                    "Please check if \"x\" is by itself, if so, add 1 as coeff~[ " +
                    inputPanel.getPolValue1() + " ]~");
        }
    }

    private void get1stPolinom() {
        try {
            polinom1 = new Polinome(inputPanel.getPolValue1());
        } catch (NumberFormatException invalidFormat) {
            Component message = null;
            JOptionPane.showMessageDialog(message,
                    "Invalid Input for 1st Polinom ~[ " + inputPanel.getPolValue1() + " ]~");
        } catch (ArrayIndexOutOfBoundsException a) {
            Component message = null;
            JOptionPane.showMessageDialog(message, "Error." +
                    "Please check if \"x\" is by itself, if so, add 1 as coeff~[ " +
                    inputPanel.getPolValue2() + " ]~");
        }
    }
}



