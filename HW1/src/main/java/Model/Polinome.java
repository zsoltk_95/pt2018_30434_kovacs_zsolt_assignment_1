package Model;


import java.util.HashMap;

public class Polinome {
    private HashMap<Integer, Double> polinome = new HashMap<Integer, Double>();

    /**
     *
     * @param string -takes String form of the polynomial which will be processed and stored
     *               in a HashMap.
     */
    public Polinome(String string) {
        string = string.replace(" ", "");
        if (string.length() > 0) {
            string = string.replace("X", "x");
            string = string.replace("-x", "-1x");
            string = string.replace("+x", "+1x");
            string = string.replace("*x", "x");
            String[] parts = string.split("(=?[+-])", -1);
            for (String part : parts) {
                calculateSubparts(part);
            }
        }
    }

    /**
     *
     * @param other - Takes a polynomial as param, adds it with the caller polynom object
     * @return returns a polynom containing the result of the addition
     */
    public Polinome add(Polinome other) {
        Polinome result = new Polinome("");

        for (int i : polinome.keySet()) {
            result.polinome.put(i, polinome.get(i));
        }

        for (int i : other.polinome.keySet()) {
            if (result.polinome.containsKey(i)) {
                result.polinome.put(i, result.polinome.get(i) + other.polinome.get(i));
            } else {
                result.polinome.put(i, other.polinome.get(i));
            }
        }

        return result;
    }
    /**
     *
     * @param other - Takes a polynomial as param, substracts it from the caller polynom object
     * @return returns a polynom containing the result of thesubstraction
     */
    public Polinome subtract(Polinome other) {
        Polinome result = new Polinome("");

        for (int i : polinome.keySet()) {
            result.polinome.put(i, polinome.get(i));
        }

        for (int i : other.polinome.keySet()) {
            if (result.polinome.containsKey(i)) {
                result.polinome.put(i, result.polinome.get(i) - other.polinome.get(i));
            } else {
                result.polinome.put(i, -other.polinome.get(i));
            }
        }

        return result;
    }
    /**
     *
     * @param other - Takes a polynomial as param, multiplies it with the caller polynom object
     * @return returns a polynom containing the result of multiplication
     */
    public Polinome multiply(Polinome other) {
        Polinome result = new Polinome("");

        for (int i : polinome.keySet()) {
            for (int j : other.polinome.keySet()) {
                if (result.polinome.containsKey(i + j)) {
                    result.polinome.put(i + j, result.polinome.get(i + j) + other.polinome.get(i) * polinome.get(j));
                } else {
                    result.polinome.put(i + j, polinome.get(i) * other.polinome.get(j));
                }
            }
        }

        return result;
    }
    /**
     *
     * @return returns a polynom obrained from derivating the caller polynomial
     */
    public Polinome derivate() {
        Polinome result = new Polinome("");

        for (int i : polinome.keySet()) {
            if (i > 0) {
                result.polinome.put(i - 1, polinome.get(i) * i);
            }
        }

        return result;
    }
    /**
     *
     * @return returns a polynom obtained from integrating the caller polynomial
     */
    public Polinome integrate() {
        Polinome result = new Polinome("");

        for (int i : polinome.keySet()) {
            result.polinome.put(i + 1, polinome.get(i) / (i + 1));
        }

        return result;
    }

    /**
     *
     * @param part -takes the subparts of the String form of the polynomial, like 2x^2, then
     *             extracts the coefficient and the power which will be stored in a Hashmap as (power,coeff)
     */
    private void calculateSubparts(String part) {
        double a;
        int n;
        //2x^2
        String[] subpart = part.split("x\\^");
        if (subpart[0].equals(null)) {
            a = 1.0;
            n = Integer.parseInt(subpart[1]);
        } else if (subpart.length == 2 && !subpart[0].equals(null)) {
            a = Double.parseDouble(subpart[0]);
            n = Integer.parseInt(subpart[1]);
        } else {
            if (part.contains("x")) {
                a = Double.parseDouble(part.split("x")[0]);  // hiba ha x csak onmagaban van
                n = 1;
            } else {
                a = Double.parseDouble(part);
                n = 0;
            }
        }
        polinome.put(n, a);
    }

    public String toString() {
        String result = "";
        double a;
        for (int i : polinome.keySet()) {
            a = polinome.get(i);
            if (a != 0) {
                if (a > 0) {
                    result = "+" + a + "x^" + i + result;
                } else {
                    result = a + "x^" + i;
                }
            }
        }
        return result;
    }
}

