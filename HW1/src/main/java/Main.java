import Controller.Controller;

/**
 * This program implements a simple arithmetic Calculator for polynomials
 * @author Zsolt Kovacs
 */
public class Main {
    public static void main(String[] args) {
        Controller app = new Controller();
    }
}
