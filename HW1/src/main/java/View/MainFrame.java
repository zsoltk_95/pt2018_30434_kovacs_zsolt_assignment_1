package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {
    private InputPanel inputPanel;
    private JTextArea textarea;
    private JButton calculateBtn;

    public MainFrame(String title, InputPanel inputPanel) {

        super(title);
        //Layout Manager
        setLayout(new BorderLayout());
        //Swing Components
        inputPanel = inputPanel;
        textarea = new JTextArea(">> Steps << \n-Write your polynomials\n" +
                "-Select the operation from the ComboBox \n" + "-Press Process Input and then Calculate\n"
                + "-Sample input:[ 2x^2+3x+1 ]");
        textarea.setBounds(10, 30, 200, 200);
        textarea.setBackground(Color.black);
        textarea.setForeground(Color.white);
        textarea.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 5, true));

        calculateBtn = new JButton("Calculate");
        calculateBtn.setBounds(1, 1, 2, 1);
        //Add Swing Components to contetnt pane
        Container c = getContentPane();
        c.add(textarea, BorderLayout.CENTER);
        c.add(calculateBtn, BorderLayout.SOUTH);
        c.add(inputPanel, BorderLayout.WEST);
    }

    public JTextArea getTextarea() {
        return textarea;
    }

    public void appendToTextarea(String text) {
        this.textarea.append(text);
    }

    public void setTextarea(String text) {
        this.textarea.setText(text);
    }

    public void setCalculateBtnListener(ActionListener listener) {
        this.calculateBtn.addActionListener(listener);
    }
}
