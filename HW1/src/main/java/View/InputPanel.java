package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class InputPanel extends JPanel {
    JTextField polValue1 = new JTextField(10);
    JComboBox operationsSelectBtn;
    JTextField polValue2 = new JTextField(10);
    String[] operations = {"Display", "Addition", "Substract", "Multiply", "Derivate", "Integrate"};

    public InputPanel() {
        Dimension size = getPreferredSize();
        size.width = 250;
        setPreferredSize(size);
        setBorder(BorderFactory.createTitledBorder("Input Data"));

        JLabel polLabel1 = new JLabel("1st Plynomial");
        JLabel polLabel2 = new JLabel("2nd Polynomial");

        polValue1 = new JTextField(10);
        polValue2 = new JTextField(10);

        operationsSelectBtn = new JComboBox(operations);


        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();
        ////First Column///
        gc.anchor = GridBagConstraints.CENTER;
        gc.weightx = 0.5;
        gc.weighty = 0.5;
        gc.gridx = 0;
        gc.gridy = 0;
        add(polLabel1, gc);
        gc.gridx = 0;
        gc.gridy = 1;
        add(polLabel2, gc);

        ///2nd Column//

        gc.gridx = 1;
        gc.gridy = 0;
        add(polValue1, gc);
        gc.gridx = 1;
        gc.gridy = 1;
        add(polValue2, gc);

        //final row
        gc.anchor = GridBagConstraints.FIRST_LINE_END;
        gc.weighty = 3;
        gc.gridx = 1;
        gc.gridy = 2;
        gc.gridx = 0;
        gc.gridy = 2;
        add(operationsSelectBtn, gc);


    }

    public String getPolValue1() {
        return polValue1.getText();
    }

    public void setPolValue1(JTextField polValue1) {
        this.polValue1 = polValue1;
    }

    public String getoperationsSelectBtn() {
        return (String) operationsSelectBtn.getSelectedItem();
    }

    public void setoperationsSelectBtn(JComboBox operationsSelectBtn) {
        this.operationsSelectBtn = operationsSelectBtn;
    }

    public String getPolValue2() {
        return polValue2.getText();
    }

    public void setPolValue2(JTextField polValue2) {
        this.polValue2 = polValue2;
    }

    public void setOperationsSelectBtnActionlistener(ActionListener listener) {
        this.operationsSelectBtn.addActionListener(listener);
    }
}
