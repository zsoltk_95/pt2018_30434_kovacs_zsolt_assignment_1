import Model.Polinome;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JUnitTests {
    public Polinome polinom1;
    public Polinome polinom2;
    @BeforeClass
    public static void setupClass() {
    }

    @Before
    public void setUp() throws Exception {

    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowInvalidException(){
        polinom1=new Polinome("thisIsNotAPolinome");
    }
    @Test
    public void addition(){
        polinom1=new Polinome("2x");
        polinom2=new Polinome("1");
        assertEquals("+2.0x^1+1.0x^0",polinom1.add(polinom2).toString());
    }
    @Test
    public void substraction() throws Exception {
        polinom1=new Polinome("2x+1");
        polinom2=new Polinome("1");
        assertEquals("+2.0x^1",polinom1.subtract(polinom2).toString());
    }

    @Test
    public void multiply(){
        polinom1=new Polinome("2x^2+1");
        polinom2=new Polinome("2");
        assertEquals("+4.0x^2+2.0x^0",polinom1.multiply(polinom2).toString());
    }
    @Test
    public void derivate(){
        polinom1=new Polinome("2x^2+1");
        assertEquals("+4.0x^1",polinom1.derivate().toString());
    }

    @Test
    public void integrate(){
        polinom1=new Polinome("1x+1");
        assertEquals("+0.5x^2+1.0x^1",polinom1.integrate().toString());
    }
}
